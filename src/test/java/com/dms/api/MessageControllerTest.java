package com.dms.api;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.dms.api.model.MessageMetadata;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {

    //TODO: im mixing unit and integration tests by doing this :(
    //https://www.baeldung.com/java-spring-mockito-mock-mockbean
    @Autowired
    private MockMvc mvc;

    @MockBean
    private Storage gcStorage;

    @MockBean
    private MessageIdService messageIdService;

    @Value("${gc-bucket-name}")
    private String gcBucketName;

    @Value("${gc-bucket-message-id-folder-name}")
    private String gcBucketMessageIdFolderName;

    @Value("${gc-bucket-folder-name}")
    private String gcBucketFolderName;

    private ObjectMapper mapper;


    public MessageControllerTest() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @BeforeEach
    public void beforeEach() throws Exception {
    }

    @AfterEach
    public void afterEach() throws Exception {
    }


    @Test
    public void getMessage() throws Exception {
        MessageMetadata messageFixture = Fixtures.messageFixture2;
        String messageMetadataContent = mapper.writeValueAsString(messageFixture);

        Blob messageBlob = Mockito.mock(Blob.class);
        Mockito.when(messageBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(messageMetadataContent.getBytes("UTF-8"));
        Mockito.when(gcStorage.get(ArgumentMatchers.any(BlobId.class))).thenReturn(messageBlob);

        mvc.perform(MockMvcRequestBuilders
                .get("/message")
                .queryParam("id", messageFixture.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(messageFixture.getId())))
                .andExpect(jsonPath("$.email", is(messageFixture.getEmail())))
                .andExpect(jsonPath("$.contentLocation", is(messageFixture.getContentLocation())))
                .andExpect(jsonPath("$.scheduledDelivery", is(messageFixture.getScheduledDelivery().format(DateTimeFormatter.ISO_DATE_TIME))))
                .andExpect(jsonPath("$.messageRescheduleMinutes", is(Integer.valueOf(messageFixture.getMessageRescheduleMinutes().toString()))));
    }

    @Test
    public void getMessageWhenMessageIdDoesNotExist() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/message")
                .queryParam("id", "NoSuchId")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("message metadata not found for id"));
    }

    @Test
    public void getMessageContent() throws Exception {
        final MessageMetadata messageFixture = Fixtures.messageFixture3;
        final String content = "FTP and I dont mean file transfer";

        Blob contentBlob = Mockito.mock(Blob.class);
        Mockito.when(contentBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(content.getBytes("UTF-8"));
        Mockito.when(gcStorage.get(ArgumentMatchers.any(BlobId.class))).thenReturn(contentBlob);

        String messageMetadataContent = mapper.writeValueAsString(messageFixture);

        Blob metadataBlob = Mockito.mock(Blob.class);
        Mockito.when(metadataBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(messageMetadataContent.getBytes("UTF-8"));

        BlobId blobId = BlobId.of(gcBucketName, gcBucketMessageIdFolderName + "/" + messageFixture.getId());
        Mockito.when(gcStorage.get(blobId)).thenReturn(metadataBlob);

        mvc.perform(MockMvcRequestBuilders
                .get("/message/content")
                .queryParam("id", messageFixture.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(content)));
    }

    @Test
    public void getMessageContentWhenMessageIdDoesNotExist() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/message/content")
                .queryParam("id", "NoSuchId")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("message metadata not found for id"));
    }

    @Test
    public void getMessageContentWhenMessageContentDoesNotExist() throws Exception {
        MessageMetadata messageFixture = Fixtures.messageFixture1;

        Mockito.when(gcStorage.get(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(null);

        String messageMetadataContent = mapper.writeValueAsString(messageFixture);

        Blob metadataBlob = Mockito.mock(Blob.class);
        Mockito.when(metadataBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(messageMetadataContent.getBytes("UTF-8"));

        BlobId blobId = BlobId.of(gcBucketName, gcBucketMessageIdFolderName + "/" + messageFixture.getId());
        Mockito.when(gcStorage.get(blobId)).thenReturn(metadataBlob);

        Mockito.when(gcStorage.get(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders
                .get("/message/content")
                .queryParam("id", messageFixture.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("message content not found for id"));
    }

    @Test
    public void rescheduleMessage() throws Exception {
        //TODO: finish writing this test

        //arrange
//        String messageContentFixture = "content is really irrelevent? here!";
//        MessageMetadata messageMetadataFixture = Fixtures.messageFixture1;
//        final LocalDateTime rescheduledDeliveryFixture = messageMetadataFixture.getScheduledDelivery().plusMinutes(10000);
//
//        String messageMetadataContent = mapper.writeValueAsString(messageMetadataFixture);
//
//        // set the metadata returned from gc storage to a known value
//        Blob metadataBlob = Mockito.mock(Blob.class);
//        Mockito.when(metadataBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(
//                messageMetadataContent.getBytes("UTF-8"));
//        BlobId blobIdMetadata = BlobId.of(gcBucketName, gcBucketMessageIdFolderName + "/" + messageMetadataFixture.getId());
//        Mockito.when(gcStorage.get(blobIdMetadata)).thenReturn(metadataBlob);
//
//        // set the content returned from gc storage to a known value
//        Blob messageContentBlob = Mockito.mock(Blob.class);
//        Mockito.when(messageContentBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(
//                messageContentFixture.getBytes("UTF-8"));
//        String existingMessageContentObjectName = GoogleCloudMessageContentStorageService.getMessageObjectName(gcBucketFolderName, messageMetadataFixture);
//        BlobId blobIdContent = BlobId.of(gcBucketName, existingMessageContentObjectName);
//        Mockito.when(gcStorage.get(blobIdContent)).thenReturn(messageContentBlob);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("scheduledDelivery", rescheduledDeliveryFixture.format(DateTimeFormatter.ISO_DATE_TIME));
//
//        //act
//        mvc.perform(MockMvcRequestBuilders
//                .put("/message/reschedule")
//                .queryParam("id", messageMetadataFixture.getId())
//                .headers(headers)
//                .accept(MediaType.APPLICATION_JSON))
//
//        //assert
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(messageMetadataFixture.getId())));

    }

    @Test
    public void acknowledgeMessage() throws Exception {
        final String email = "putmessagetest@internet.org";
        final String content = "its a series of tubes";
        final LocalDateTime scheduledDelivery = LocalDateTime.now();

        //TODO: finish writing this test

    }

    @Test
    public void postMessage() throws Exception {
        final String email = "postmessagetest@yehaw.com";
        final String content = "this is my super secret love confession that must only be sent out if I am no longer a party to this existence";
        final LocalDateTime scheduledDelivery = LocalDateTime.now();
        final String messageId = UUID.randomUUID().toString().replace("-", "");
        final Long messageRescheduleMinutes = 20000l;

        MessageMetadata message = new MessageMetadata();
        message.setId(messageId);
        message.setScheduledDelivery(scheduledDelivery);
        message.setEmail(email);
        message.setContentLocation(GoogleCloudMessageContentStorageService.getMessageObjectName(gcBucketFolderName, message));
        message.setMessageRescheduleMinutes(messageRescheduleMinutes);

        // set the message id to a known value
        Mockito.when(messageIdService.generateMessageId()).thenReturn(message.getId());

        // set the content returned from gc storage to a known value
        Blob contentBlob = Mockito.mock(Blob.class);
        Mockito.when(contentBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(content.getBytes("UTF-8"));
        BlobId blobIdContent = BlobId.of(gcBucketName, GoogleCloudMessageContentStorageService.getMessageObjectName(gcBucketFolderName, message));
        BlobInfo blobInfoContent = BlobInfo.newBuilder(blobIdContent).build();
        Mockito.when(gcStorage.create(blobInfoContent, content.getBytes("UTF-8") )).thenReturn(contentBlob);

        String messageMetadataContent = mapper.writeValueAsString(message);

        // set the metadata returned from gc storage to a known value
        Blob metadataBlob = Mockito.mock(Blob.class);
        Mockito.when(metadataBlob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(messageMetadataContent.getBytes("UTF-8"));
        BlobId blobIdMetadata = BlobId.of(gcBucketName, gcBucketMessageIdFolderName + "/" + message.getId());
        BlobInfo blobInfoMetadata = BlobInfo.newBuilder(blobIdMetadata).build();
        Mockito.when(gcStorage.create(blobInfoMetadata, messageMetadataContent.getBytes("UTF-8") )).thenReturn(metadataBlob);

        HttpHeaders headers = new HttpHeaders();
        headers.add("content", content);
        headers.add("email", email);
        headers.add("scheduledDelivery", scheduledDelivery.format(DateTimeFormatter.ISO_DATE_TIME));
        headers.add("messageRescheduleMinutes", messageRescheduleMinutes.toString());

        mvc.perform(MockMvcRequestBuilders
                .post("/message")
                .headers(headers)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.email", is(email)))
                .andExpect(jsonPath("$.scheduledDelivery", is(scheduledDelivery.format(DateTimeFormatter.ISO_DATE_TIME))))
                .andExpect(jsonPath("$.messageRescheduleMinutes", is(Integer.valueOf(messageRescheduleMinutes.toString()))));

        Mockito.verify(gcStorage, Mockito.times(2)).create(ArgumentMatchers.any(BlobInfo.class), ArgumentMatchers.any(byte[].class));
        Mockito.verify(contentBlob, Mockito.times(1)).getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class));
        Mockito.verify(metadataBlob, Mockito.times(1)).getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class));
    }

//    @Test
//    public void postMessageWhenMessageSaveFails() throws Exception {
//        final String email = "postmaster@theweb.tubes";
//        final String content = "contentMyContent";
//        final LocalDateTime scheduledDelivery = LocalDateTime.now();
//
//        //TODO: make sure the content is removed from google storage
//        Blob blob = Mockito.mock(Blob.class);
//        Mockito.when(blob.getContent(ArgumentMatchers.any(Blob.BlobSourceOption.class))).thenReturn(content.getBytes("UTF-8"));
//        Mockito.when(gcStorage.create(ArgumentMatchers.any(BlobInfo.class), ArgumentMatchers.any(byte[].class) )).thenReturn(blob);
//
//        Mockito.when(messageRepository.save(ArgumentMatchers.any(Message.class))).thenThrow(new IllegalArgumentException());
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("content", content);
//        headers.add("email", email);
//        headers.add("scheduledDelivery", scheduledDelivery.format(DateTimeFormatter.ISO_DATE_TIME));
//
//        mvc.perform(MockMvcRequestBuilders
//                .post("/message")
//                .headers(headers)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isInternalServerError())
//                .andExpect(status().reason("failed to save message - please try again"));
//    }

//    @Test
//    public void postMessageWhenGoogleStorageSaveThrowsStorageException() throws Exception {
//        //TODO: make sure we roll back the insert into the database for this record
//    }

    //TODO: refactor test
//    @Test
//    public void deleteMessage() throws Exception {
//        final String id = Fixtures.messageFixture1.getId();
//
//        Long initialCount = messageRepository.count();
//
//        mvc.perform(MockMvcRequestBuilders
//                .delete("/message")
//                .queryParam("id", id)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(id)));
//
//        Long finalCount = messageRepository.count();
//        assertThat(finalCount, is(equalTo(initialCount - 1)));
//
//        List<Message> messages = new ArrayList<>();
//        messageRepository.findAll().iterator().forEachRemaining(messages::add);
//
//        //check that the message id is not present in the dataset
//        messages.forEach(message -> {
//            assertThat(message.getId(), not(id));
//        });
//    }

}
