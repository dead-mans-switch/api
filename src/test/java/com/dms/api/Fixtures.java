package com.dms.api;

import com.dms.api.model.MessageMetadata;

import java.time.LocalDateTime;

public class Fixtures {
    private Fixtures() {
        throw new UnsupportedOperationException();
    }

    public static MessageMetadata messageFixture1 = new MessageMetadata("392ca1c0227049049eefe113c42f4a86",
            "messages/2020/5/1/20/392ca1c0227049049eefe113c42f4a86", "me@myemail.org",
            LocalDateTime.parse("2020-05-01T20:00:00.000000"), 30080l);

    public static MessageMetadata messageFixture2 = new MessageMetadata("4478283c1a85420aa095c0e0828dd964",
            "messages/2020/5/2/20/4478283c1a85420aa095c0e0828dd964", "test@email.com",
            LocalDateTime.parse("2020-05-02T20:00:00.000000"), 40080l);

    public static MessageMetadata messageFixture3 = new MessageMetadata("4fc4e4da2d544460bbf8c36fbee52a4e",
            "messages/2020/5/3/20/4fc4e4da2d544460bbf8c36fbee52a4e", "john@greekmail.gr",
            LocalDateTime.parse("2020-05-03T20:00:00.000000"), 50080l);
}
