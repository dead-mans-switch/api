package com.dms.api;

public interface MessageIdService {
    String generateMessageId();
}
