package com.dms.api;

import com.dms.api.model.MessageMetadata;

public interface MessageMetadataStorageService {
    MessageMetadata get(String id);
    MessageMetadata create(MessageMetadata message);
    MessageMetadata update(MessageMetadata message);
    void delete(MessageMetadata message);
}
