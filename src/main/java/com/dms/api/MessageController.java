package com.dms.api;

import com.dms.api.model.MessageMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;


@Slf4j
@RestController()
public class MessageController {

    @Autowired
    private MessageContentStorageService messageContentStorageService;

    @Autowired
    private MessageMetadataStorageService messageMetadataStorageService;

    @Autowired
    private MessageIdService messageIdService;


    /**
     * Returns one particular message metadata object based on the id provided
     * //TODO: this should eventually restrict responses to a particular user
     * @param id
     * @return
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/message")
    public MessageMetadata message(@RequestParam(value = "id") String id) {
        log.info("received request for message id: '{}'", id);
        return messageMetadataStorageService.get(id);
    }

    /**
     * Returns the contents of the message from storage
     * It finds the storage location by first querying the database for the message content location then it searches
     * the storage bucket for the message content
     * @param id
     * @return
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/message/content")
    public String messageContent(@RequestParam(value = "id") String id) {
        log.info("received request for contents of message id: '{}'", id);
        MessageMetadata message = messageMetadataStorageService.get(id);
        return messageContentStorageService.get(message);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/message/reschedule")
    public MessageMetadata rescheduleMessage(@RequestParam(value = "id") String id,
                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                             @RequestHeader("scheduledDelivery") LocalDateTime scheduledDelivery) {
        log.info("received request to reschedule an existing message");

        MessageMetadata rescheduledMessage = reschedule(id, scheduledDelivery);

        return rescheduledMessage;
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/message/acknowledge")
    public MessageMetadata acknowledgeMessage(@RequestParam(value = "id") String id) {
        //TODO: need to add a test for this method

        log.info("received request to acknowledge an existing message");

        MessageMetadata messageMetadata = messageMetadataStorageService.get(id);

        LocalDateTime newScheduledDelivery = messageMetadata.getScheduledDelivery().plusMinutes(messageMetadata.getMessageRescheduleMinutes());

        MessageMetadata rescheduledMessage = reschedule(id, newScheduledDelivery);
        return rescheduledMessage;
    }


    @CrossOrigin(origins = "*")
    @PostMapping("/message")
    public MessageMetadata message(@RequestHeader("content") String content, @RequestHeader("email") String email,
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                   @RequestHeader("scheduledDelivery") LocalDateTime scheduledDelivery,
                                   @RequestHeader(name="messageRescheduleMinutes", required=false, defaultValue="10080") Long messageRescheduleMinutes) {

        //TODO: check that the email is verified - eventually

        //TODO: check that time has not already passed or is not too soon (by some static threshold)
        //TODO: assert that the time requested is an even hour?

        //TODO: should actually return a 201

        log.info("received request to create a new message");
        MessageMetadata message = new MessageMetadata();
        message.setId(messageIdService.generateMessageId());
        message.setEmail(email);
        message.setScheduledDelivery(scheduledDelivery);
        message.setMessageRescheduleMinutes(messageRescheduleMinutes);

        MessageMetadata savedMessage = messageContentStorageService.create(message, content);

        //save message metadata to database table
        messageMetadataStorageService.create(savedMessage);
//        try {
//
//        } catch (Exception e) {
//            log.error("could not save message: {}", e.getMessage());
//            e.printStackTrace();
//            //TODO: delete the message saved to content storage service
//        }

        return savedMessage;
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/message")
    public MessageMetadata deleteMessage(@RequestParam(value = "id") String id) {
        log.info("received request to delete message id: '{}'", id);
        MessageMetadata message = messageMetadataStorageService.get(id);

        messageMetadataStorageService.delete(message);
        messageContentStorageService.delete(message);

        return message;
    }

    /**
     * Takes an existing message id, finds the message if exists and reschedules it to the new time provided
     * @param id
     * @param scheduledDelivery
     * @return
     */
    private MessageMetadata reschedule(String id, LocalDateTime scheduledDelivery) {
        MessageMetadata existingMessage = messageMetadataStorageService.get(id);

        MessageMetadata newMessage = existingMessage.clone();
        //TODO: check that new delivery date is after the currently set delivery date
        newMessage.setScheduledDelivery(scheduledDelivery);

        MessageMetadata contentMovedMessage = messageContentStorageService.update(existingMessage, newMessage);

        MessageMetadata rescheduledMessage = messageMetadataStorageService.update(contentMovedMessage);
        return rescheduledMessage;
    }

}
