package com.dms.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class RandomUUIDMessageIdService implements MessageIdService {

    @Override
    public String generateMessageId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
