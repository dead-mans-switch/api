package com.dms.api;

import com.dms.api.model.MessageMetadata;
import com.google.cloud.storage.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Slf4j
@Service
public class GoogleCloudMessageContentStorageService implements MessageContentStorageService {

    @Autowired
    private Storage gcStorage;

    @Value("${gc-bucket-name}")
    private String gcBucketName;

    @Value("${gc-bucket-folder-name}")
    private String gcBucketFolderName;

    private static final String SUPPORTED_CHARSET = "UTF-8";


    @Override
    public String get(MessageMetadata message) {
        Blob blob = gcStorage.get(BlobId.of(gcBucketName, message.getContentLocation()));
        if(blob != null) {
            try {
                return new String(blob.getContent(), SUPPORTED_CHARSET);
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "message content could not be read"
                );
            }
        } else {
            log.error("failed to find contents for message id: {}", message.getId());
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "message content not found for id"
            );
        }
    }

    @Override
    public MessageMetadata create(MessageMetadata message, String content) {
        String messageObjectName = getMessageObjectName(gcBucketFolderName, message);

        //write the message contents to google cloud storage bucket
        BlobId blobId = BlobId.of(gcBucketName, messageObjectName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

        try {
            Blob blob = gcStorage.create(blobInfo, content.getBytes(SUPPORTED_CHARSET));
            String verifyContent = new String(blob.getContent(), SUPPORTED_CHARSET);
            assert (verifyContent.contentEquals(content));
        } catch (StorageException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new MessageMetadata(message.getId(), messageObjectName, message.getEmail(),
                message.getScheduledDelivery(), message.getMessageRescheduleMinutes());
    }

    @Override
    public MessageMetadata update(MessageMetadata existing, MessageMetadata updated) {
        String existingMessageObjectName = getMessageObjectName(gcBucketFolderName, existing);
        String updatedMessageObjectName = getMessageObjectName(gcBucketFolderName, updated);

        Blob existingBlob = gcStorage.get(gcBucketName, existingMessageObjectName);
        // Write a copy of the object to the target bucket
        CopyWriter copyWriter = existingBlob.copyTo(gcBucketName, updatedMessageObjectName);
        Blob copiedBlob = copyWriter.getResult();
        // Delete the original blob now that we've copied to where we want it, finishing the "move" operation
        existingBlob.delete();

        return new MessageMetadata(updated.getId(), updatedMessageObjectName, updated.getEmail(),
                updated.getScheduledDelivery(), updated.getMessageRescheduleMinutes());
    }

    @Override
    public void delete(MessageMetadata message) {
        gcStorage.delete(gcBucketName, message.getContentLocation());
    }

    //TODO: this should probably be refactored in some way - I added it here to facilitate calls from tests
    public static String getMessageObjectName(String gcBucketFolderName, MessageMetadata message) {
        //TODO: figure out how to restrict this to UTC time only
        return gcBucketFolderName + "/" + message.getScheduledDelivery().getYear() + "/"
                + message.getScheduledDelivery().getMonthValue() + "/" + message.getScheduledDelivery().getDayOfMonth() + "/"
                + message.getScheduledDelivery().getHour() + "/" + message.getScheduledDelivery().getMinute() + "/" + message.getId();
    }
}
