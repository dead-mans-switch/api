package com.dms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.jayway.jsonpath.JsonPath;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageMetadata {
    private String id;
    private String contentLocation;
    private String email;
    private LocalDateTime scheduledDelivery;
    private Long messageRescheduleMinutes;

    /**
     * Parses a json representation of a message into a Message object and returns the Message object
     * @param json
     * @return
     */
    public static MessageMetadata fromJson(String json) {
        MessageMetadata message = new MessageMetadata();
        message.setId(JsonPath.read(json, "$.id"));
        message.setContentLocation(JsonPath.read(json, "$.contentLocation"));
        message.setEmail(JsonPath.read(json, "$.email"));
        message.setScheduledDelivery(LocalDateTime.parse(JsonPath.read(json, "$.scheduledDelivery")));
        Integer messageRescheduleMinutes = JsonPath.read(json, "$.messageRescheduleMinutes");
        message.setMessageRescheduleMinutes(Long.valueOf(messageRescheduleMinutes));
        return message;
    }

    /**
     * copies the fields of the message into a new object
     * @return
     */
    @Override
    public MessageMetadata clone() {
        MessageMetadata message = new MessageMetadata();
        message.setId(this.getId());
        message.setEmail(this.getEmail());
        message.setContentLocation(this.getContentLocation());
        message.setScheduledDelivery(this.getScheduledDelivery());
        message.setMessageRescheduleMinutes(this.getMessageRescheduleMinutes());
        return message;
    }
}
