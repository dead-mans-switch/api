package com.dms.api;

import com.dms.api.model.MessageMetadata;

public interface MessageContentStorageService {
    String get(MessageMetadata message);
    MessageMetadata create(MessageMetadata message, String content);
    MessageMetadata update(MessageMetadata existing, MessageMetadata updated);
    void delete(MessageMetadata message);
}
