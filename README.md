Dead Man's Switch API


# build docker image locally (no build cache)
docker build -t dms-api:local .

# run container
docker run --rm -d --name dms-api \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-api/secrets/dgflagg-dms-api.json \
    -v "$(pwd):/dms-api/secrets" -p 8080:8080 dms-api:local
    
docker run --rm -d --name dms-api \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-api/secrets/dgflagg-dms-api.json \
    -v "$(pwd):/dms-api/secrets" -p 8080:8080 us.gcr.io/dgflagg-dms-api/dms-api:latest
    
# swagger UI
http://localhost:8080/swagger-ui.html
http://dms.dgflagg.info:8080/swagger-ui.html

# check health status
curl http://localhost:8080/actuator/health
curl http://dms.dgflagg.info:8080/actuator/health

# create a new message
curl -X POST \
  http://localhost:8080/message \
  -H "content: message contents ${RANDOM}" \
  -H "email: dmstest@dgflagg.info" \
  -H "scheduledDelivery: $(date --iso-8601=seconds)"
  
curl -X POST \
  http://dms.dgflagg.info:8080/message \
  -H "content: message contents ${RANDOM}" \
  -H "email: dmstest@dgflagg.info" \
  -H "scheduledDelivery: $(date --iso-8601=seconds)"

# get a specific message metadata
curl http://localhost:8080/message?id=${MESSAGE_ID}
curl http://dms.dgflagg.info:8080/message?id=${MESSAGE_ID}

# get contents of a specific message
curl http://localhost:8080/message/content?id=${MESSAGE_ID}
curl http://dms.dgflagg.info:8080/message/content?id=${MESSAGE_ID}

# delete a specific message
curl -X DELETE http://localhost:8080/message?id=${MESSAGE_ID}
curl -X DELETE http://dms.dgflagg.info:8080/message?id=${MESSAGE_ID}

# reschedule a message
curl -X PUT \
    http://localhost:8080/message/reschedule?id=${MESSAGE_ID} \
    -H "scheduledDelivery: 2029-11-12T13:14:00-04:00"

curl -X PUT \
    http://dms.dgflagg.info:8080/message/reschedule?id=${MESSAGE_ID} \
    -H "scheduledDelivery: 2029-11-12T13:14:00-04:00"
    
# acknowledge a specific message
curl -X PUT http://localhost:8080/message/acknowledge?id=${MESSAGE_ID}
curl -X PUT http://dms.dgflagg.info:8080/message/acknowledge?id=${MESSAGE_ID}


# TODO:
- logging
- error handling
- meaningful comments throughout
- unit tests
- integration tests
- delete message metadata and contents once they are already sent?

# high level roadmap
* should a user be able to delete a message permanently? maybe not bc it might not be the user doing it but maybe bc
    the message no longer applies and they do not want to send it out anymore - leaning towards allowing deletion
- messages should have an optional configuration to make them:
    1) remindable - send a request to the user's email to let them know they should acknowledge the alert so it can
        be rescheduled to be sent in the future
            - this change will likely require another scheduler or change to existing scheduler to send the reminder
            - also need to update message metadata to store these details 
