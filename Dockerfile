# uses the multi-stage build pattern to  1) create the binary and  2) package the binary into an image
# use maven as the builder base image
FROM maven:3.6.3-openjdk-11 AS builder

# copy pom into the builder container
COPY pom.xml pom.xml

# copy source into the container
COPY src/main src/main

# build and test the binary
RUN mvn clean verify


# use an openjdk 11 image to package the binary
FROM adoptopenjdk/openjdk11:alpine-jre

# create the directory to store the api resources
RUN mkdir -p /dms-api

# set the app directory as the current working directory
WORKDIR /dms-api

# restrict the application permissions by creating and then running as a standard user
RUN addgroup -S dms-api && adduser -S dms-api -G dms-api

# copy only the final fat jar from the builder container
COPY --from=builder target/api-*.jar app.jar

# make the api user the owner of the app directory
RUN chown -R dms-api:dms-api /dms-api

# change to the api user
USER dms-api:dms-api

# run the application as the default entrypoint
ENTRYPOINT ["java","-jar","app.jar"]