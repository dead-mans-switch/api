#!/bin/bash

# update the docker container locally
docker pull us.gcr.io/dgflagg-dms-api/dms-api:latest

# stop the currently running api container
docker rm -f dms-api

# run the api as a docker container
docker run --rm -d --name dms-api \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-api/secrets/dgflagg-dms-api.json \
    -v "$(pwd):/dms-api/secrets" -p 8080:8080 us.gcr.io/dgflagg-dms-api/dms-api:latest